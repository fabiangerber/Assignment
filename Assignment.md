# Assignment Fullstack Candidate

### Purpose and Goal of Assignment

The assignment tests the candidate's ability to create a modern front-end and a simplistic back-end service. The
front-end needs to fulfill a non-functional requirement in conjunction with a limited back-end. The back-end itself
shall combine two existing services and produce a dedicated output for the front-end.

### Estimated Time

6-10 hours.

### Requirement Definition Front-End

The front-end shall provide an input field to enter a city name. Corresponding to a valid city name, a satellite image
shows up below the search input field - showing the satellite image of the city. The satellite image source url is
provided via a JSON response from the back-end service. The back-end service should only be called when the user stops
typing for 1 second when the city name input field is focused. This measurement is to reduce the pressure on the
back-end.

### Requirement Definition Back-end

The service must provide a Get Request URL with passing a city name as a parameter like the following:
http://localhost/test_service?city_name=Basel
As a result, a JSON response including a link to a satellite image of the city must be provided like this:
{
"
city_image_url": "https://earthengine.googleapis.com/v1alpha/projects/earthengine-legacy/thumbnails/bde2fd5685c5eca8ec85cd6d2aef344d-7be88c838843d7ef7a4700948ded8316:getPixels"
}

### Boundaries, Hints and Further Requirements

1. Use the Bing Maps REST services to translate a city name to its coordinates. Here is a hint to resolve a place to its
   coordinates
   https://docs.microsoft.com/en-us/bingmaps/rest-services/locations/find-a-location-by-query
   For this assignment, only simple searches like "Basel", "Zurich" or "Munich" have to work. You do not need to handle
   more complicated inquiries.
2. When you have the decimal longitude and latitude coordinates of a city, you can use the NASA open API to get an URL
   to a satellite image of the requested place. Here is a hint to the NASA Open API: https://api.nasa.gov/
   The API service you want to use is called "Imagery" under the section "Earth". To use both APIs, you will need to
   create API keys that can be freely created in this case. Although it is not necessary to solve the task, you might
   want to provide a solution for not checking in your personalized API keys to GitHub.

### Technology Stack

Use Angular on the front-end. To realize the 1-second-user-debounce requirement use RxJs. There is an already existing
operator perfect for the job to control the user's input rate. You are free to choose one of the following programming
platforms for the back-end:

- Java with Spring Boot
- node.js with TypeScript plus any valid framework (e.g., express or nest.js)
- Go (Lang) and any concurrent framework

### Formalities

- The code must be readable and follow the general rules of clean code.
- Provide your project available on GitHub, GitLab or Bitbucket.
- It is not necessary to provide a running instance of the service publicly available.
- It is not mandatory to provide documentation for your project to pass the assignment.
- It is not necessary to provide presentation slides.
