package com.assignment.backend.imageService.location;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LocationResource {
    private String name;
    private Point point;

    public String getName() {
        return name;
    }

    public Point getPoint() {
        return point;
    }

    public double getLatitude() {
        return this.point.getLatitude();
    }

    public double getLongitude() {
        return this.point.getLongitude();
    }
}
