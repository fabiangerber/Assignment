package com.assignment.backend.imageService.location;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Service
public class LocationService {
    @Value("${credentials.bingMapsApiKey}")
    private String bingMapsKey;
    private final String BASE_URL_COORDINATE_SERVICE = "http://dev.virtualearth.net/REST/v1/Locations/";
    private final WebClient webClient;

    public LocationService(WebClient.Builder webClientBuilder) {
        this.webClient = webClientBuilder.baseUrl(BASE_URL_COORDINATE_SERVICE).build();
    }

    public LocationServiceResponse searchForLocation(String cityName) {
        return this.webClient.get().uri(uriBuilder -> uriBuilder
                        .path(cityName)
                        .queryParam("maxResults", 5)
                        .queryParam("key", bingMapsKey)
                        .build())
                .retrieve()
                .onStatus(httpStatus -> httpStatus.value() != HttpStatus.OK.value(),
                        response -> Mono.error(new BadGatewayException()))
                .bodyToMono(LocationServiceResponse.class).block();
    }
}
