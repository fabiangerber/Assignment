package com.assignment.backend.imageService;

import com.assignment.backend.imageService.location.LocationResource;
import com.assignment.backend.imageService.location.LocationService;
import com.assignment.backend.imageService.location.LocationServiceResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import java.time.LocalDate;

@RestController
public class ImageService {
    @Autowired
    private LocationService locationService;
    @Value("${credentials.nasaApiKey}")
    private String nasaKey;
    private final String BASE_URL_IMAGE_SERVICE = "https://api.nasa.gov/planetary/earth/imagery";

    @CrossOrigin
    @GetMapping("/image_service")
    public CityNameResponse getSatelliteImage(@RequestParam(value = "city_name", defaultValue = "Zürich") String cityName) {
        LocationServiceResponse response = locationService.searchForLocation(cityName);

        LocationResource firstLocation = response.getFirstLocation();
        String imageURL = UriComponentsBuilder.newInstance()
                .path(BASE_URL_IMAGE_SERVICE)
                .queryParam("lon", firstLocation.getLongitude())
                .queryParam("lat", firstLocation.getLatitude())
                .queryParam("date", LocalDate.now().minusMonths(1).toString()) // Fixme: Nasa API doesn't work correctly when date is to close to current.
                .queryParam("dim", 0.2)
                .queryParam("api_key", nasaKey)
                .build().toString();

        return new CityNameResponse(imageURL);
    }
}
