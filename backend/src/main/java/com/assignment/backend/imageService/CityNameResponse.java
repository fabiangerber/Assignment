package com.assignment.backend.imageService;

public class CityNameResponse {
    private final String city_image_url;

    public CityNameResponse(String city_image_url) {
        this.city_image_url = city_image_url;
    }

    public String getCity_image_url() {
        return city_image_url;
    }
}
