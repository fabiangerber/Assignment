package com.assignment.backend.imageService.location;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LocationServiceResponse {
    private String authenticationResultCode;
    private int statusCode;
    private ResourceSets[] resourceSets;

    public String getAuthenticationResultCode() {
        return authenticationResultCode;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public ResourceSets[] getResourceSets() {
        return resourceSets;
    }

    public LocationResource getFirstLocation() {
        if (resourceSets == null || resourceSets.length <= 0) {
            throw new NoResultsFoundException();
        }
        return resourceSets[0].getFirstLocation();
    }
}
