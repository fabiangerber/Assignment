package com.assignment.backend.imageService.location;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ResourceSets {
    private int estimatedTotal;
    private LocationResource[] resources;

    public int getEstimatedTotal() {
        return estimatedTotal;
    }

    public LocationResource[] getResources() {
        return resources;
    }

    public LocationResource getFirstLocation() {
        if (resources == null || resources.length <= 0) {
            throw new NoResultsFoundException();
        }
        return resources[0];
    }
}
