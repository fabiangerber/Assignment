# Backend

## Requirements

In order to run the backend you need to provide different keys. Information how to create the keys can be found in the
corresponding section below.

Duplicate the `application.properties.template` file under `./src/main/resources` and remove the `.template`
ending. Replace the following values with your personal keys:

- credentials.bingMapsApiKey
- credentials.nasaApiKey

### Generating API Keys

#### Bing Maps

To resolve coordinates of a location, the bing maps are used. An API-key can be created
[here](https://docs.microsoft.com/en-us/bingmaps/getting-started/bing-maps-dev-center-help/getting-a-bing-maps-key).

#### NASA

A key to retrieve satelite images can be generated here: [https://api.nasa.gov](https://api.nasa.gov)

## How to use

```shell
# Run backend
./mvnw spring-boot:run
```
