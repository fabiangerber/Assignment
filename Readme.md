# Assignment Fullstack Candidate

## Description

This project contains the source code for the frontend and backend for the assignment. The assignment description can be
found here: [Assignment description](./Assignment.md).

Please consult also the specific documentation:
- [Backend Documentation](./backend/Backend.md)
- [Frontend Documentation](./frontend/README.md)

## Quickstart

Create your own `./backend/src/main/resources/application.properties` file with API keys.

### Start Backend

```shell
cd backend/ && ./mvnw spring-boot:run
```

### Start Frontend

```shell
cd frontend/ && npm install && ng serve --open
```
