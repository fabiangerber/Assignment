import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SatelliteImageSearchComponent } from './satellite-image-search.component';

describe('SatelliteImageSearchComponent', () => {
  let component: SatelliteImageSearchComponent;
  let fixture: ComponentFixture<SatelliteImageSearchComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SatelliteImageSearchComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SatelliteImageSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
