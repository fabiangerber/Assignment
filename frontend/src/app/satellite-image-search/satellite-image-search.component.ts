import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Component, Injectable, OnInit } from "@angular/core";
import { FormControl } from "@angular/forms";
import { debounceTime, distinctUntilChanged, Subscription } from "rxjs";
import { environment } from "src/environments/environment";
import { ImageServiceResponse } from "./ImageServiceResponse";

@Component({
  selector: "app-satellite-image-search",
  templateUrl: "./satellite-image-search.component.html",
  styleUrls: ["./satellite-image-search.component.css"],
})
@Injectable()
export class SatelliteImageSearchComponent implements OnInit {

  readonly title: string = "Satellite Image Search";
  searchCityName: FormControl = new FormControl();
  cityImageSource: string | undefined;
  errorMessage: string | undefined;

  searchSubscription: Subscription = new Subscription();

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    this.searchSubscription =
      this.searchCityName.valueChanges.pipe(
        debounceTime(1000),
        distinctUntilChanged(),
      ).subscribe(newCityName => {
        this.resolveImageUrlFromName(newCityName);
      });
  }

  ngOnDestroy(): void {
    this.searchSubscription.unsubscribe();
  }

  private resolveImageUrlFromName(cityName: string): void {
    const path = environment.imageServiceEndpoint + `?city_name=${ cityName }`;
    const observable = this.http.get<ImageServiceResponse>(path);

    observable.subscribe(
      (response) => {
        this.errorMessage = undefined;
        this.cityImageSource = response.city_image_url;
      },
      (error: HttpErrorResponse) => {
        this.cityImageSource = undefined;
        this.errorMessage = `Unable to retrieve image for ${ cityName }: ${ error.error.status } ${ error.error.error }`;
        console.error(`Request for '${ cityName }' failed with error: ${ error.message }`);
      });
  }
}
